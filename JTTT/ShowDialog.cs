﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Table("ShowDialog")]
    class ShowDialog : Action
    {
        public override void Execute(string data)
        {
            Dialog dialog = new Dialog(data);
            dialog.Show();
        }
    }
}
