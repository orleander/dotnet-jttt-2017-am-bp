﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace JTTT
{
    [Table("SendMail")]
    public class SendMail : Action
    {
        public string target { set; get; }

        public SendMail()
        {

        }

        public SendMail(string target)
        {            
            this.target = target;
        }

        public override void Execute(string content)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("JTTT", "jttt-pwr@wp.pl"));
            message.To.Add(new MailboxAddress(target));
            message.Subject = "JTTT";
            message.Body = new TextPart("html")
            {
                Text = content
            };            

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                client.Connect("smtp.wp.pl", 465, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("jttt-pwr", "12345678aA");
                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
}