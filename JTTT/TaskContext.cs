﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class TaskContext : DbContext
    {
        public TaskContext() : base("tasks")
        {

        }

        public DbSet<Task> tasks { get; set; }
    }
}
