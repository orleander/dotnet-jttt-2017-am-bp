﻿using Org.BouncyCastle.Asn1.Crmf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace JTTT
{

    public partial class MainWindow : Window
    {
        private const string DB_PATH = "db.xml";       
        
        public MainWindow()
        {
            InitializeComponent();
            using (var context = new TaskContext())
            {
                if (context != null && context.tasks != null)
                {
                    foreach (var task in context.tasks)
                        actionsListBox.Items.Add(task);
                }
            }
        }

        // wykonuje sie w momencie klikniecia przycisku sender - mamy dodawac do listy wiadomosci
        private void execute_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new TaskContext())
            {
                foreach (var task in context.tasks)
                    task.Execute();
            }
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {            
            Condition condition = SelectedCondition();                    
            Action action = SelectedAction();
            Task task = new Task(desc.Text, condition, action);
            actionsListBox.Items.Add(task.ToString());
            using (var context = new TaskContext())
            {
                context.tasks.Add(task);
                context.SaveChanges();
            }
        }

        private void clean_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new TaskContext())
            {
                // https://stackoverflow.com/questions/10448684/how-should-i-remove-all-elements-in-a-dbset
                context.Database.ExecuteSqlCommand("delete from tasks");
                context.SaveChanges();
            }
            actionsListBox.Items.Clear();
        }

        private void weather_Click(object sender, RoutedEventArgs e)
        {            
            WeatherWindow window = new WeatherWindow();
            window.Show();
        }

        private Condition SelectedCondition()
        {
            switch(conditions.SelectedIndex)
            {
                case 0:
                    return new TextImage(url.Text, text.Text);
                case 1:
                    return new WeatherCondition(city.Text, Int32.Parse(temperature.Text));
            }

            return null;
        }

        private Action SelectedAction()
        {
            switch(actions.SelectedIndex)
            {
                case 0:
                    return new SendMail(email.Text);
                case 1:
                    return new ShowDialog();
            }

            return null;
        }
    }
}
