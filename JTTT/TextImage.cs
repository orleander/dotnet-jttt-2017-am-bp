﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace JTTT
{
    [Table("TextImage")]
    public class TextImage : Condition
    {
        public string url { set; get; }
        public string text { set; get; }
        
        private List<String> imageUrls;

        public TextImage()
        {

        }

        public TextImage(string url, string text)
        {
            this.url = url;
            this.text = text;            
        }

        public override bool CheckCondition()
        {
            Parse();
            Console.WriteLine(imageUrls.Count);
            return imageUrls.Count > 0;
        }

        public override string GetData()
        {
            var body = "";
            foreach (var imageUrl in imageUrls)
                body += "<img src=\"" + imageUrl + "\" /><br />";
            return body;
        }

        private List<string> Parse()
        {            
            var document = new HtmlDocument();
            document.LoadHtml(GetHtml());
            var nodes = document.DocumentNode.Descendants("img");

           imageUrls = new List<string>();
            foreach(var node in nodes)
            {
                if (node.GetAttributeValue("title", "").ToLower().Contains(text.ToLower()) || 
                    node.GetAttributeValue("alt", "").ToLower().Contains(text.ToLower()))
                    imageUrls.Add(node.GetAttributeValue("src", ""));
            }
            return imageUrls;
        }

        private string GetHtml()
        {
            using (var webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                return WebUtility.HtmlDecode(webClient.DownloadString(url));
            }
        }
    }
}