﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;

namespace JTTT
{
    public static class BinarySerializer
    {
        public static string FilePath = "data.dat";

        public static void Serialize(object obj)
        {
            FileStream fileStream = new FileStream(FilePath, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, obj);
            fileStream.Close();
        }

        public static Object Deserialize()
        {
            Object obj = null;
            FileStream fileStream = new FileStream(FilePath, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            obj = binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            return obj;
        }
    }

    public static class JsonSerializer
    {
        public static string FilePath = "db.json";

        public static void WriteToJsonFile<T>(T objectToWrite) where T : new()
        {
            TextWriter writer = null;
            try
            {
                var contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite);
                writer = new StreamWriter(FilePath);
                writer.Write(contentsToWriteToFile);
            }
            finally
            {
                writer?.Close();
            }
        }

        public static T ReadFromJsonFile<T>() where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(FilePath);
                var fileContents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(fileContents);
            }
            finally
            {
                reader?.Close();
            }
        }
    }
}
