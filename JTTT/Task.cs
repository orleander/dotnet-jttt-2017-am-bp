﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JTTT
{
    [Serializable]
    public class Task
    {
        public int Id { set; get; }
        public string name { set; get; }
        public virtual Condition condition { set; get; }
        public virtual Action action { set; get; }
        
        public Task()
        {

        }        

        public Task(string name, Condition condition, Action action)
        {
            this.name = name;
            this.condition = condition;
            this.action = action;
        }

        public void Execute()
        {
            if (condition.CheckCondition())
                action.Execute(condition.GetData());
        }

        public override string ToString()
        {
            return name;
        }
    }
}
