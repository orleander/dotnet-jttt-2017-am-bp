﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Table("WeatherCondition")]
    class WeatherCondition : Condition
    {
        public string city { set; get; }
        public int temperature { set; get; }

        public WeatherCondition()
        {

        }

        public WeatherCondition(string city, int temperature)
        {
            this.city = city;
            this.temperature = temperature;
        }

        public override bool CheckCondition()
        {
            var weather = OpenWeather.GetWeatherForCity(city);
            return OpenWeather.ToCelsius(weather.main.temp) >= temperature;
        }

        public override string GetData()
        {
            var weather = OpenWeather.GetWeatherForCity(city);
            string data = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" /><h3>"+city+"</h3><hr />Teraz jest <b>" + OpenWeather.ToCelsius(weather.main.temp) + "</b> °C. Ciśnienie <b>" + weather.main.pressure + "</b> hPa. Pogoda: <b>" + weather.weather[0].description + "</b>.";
            data += "<br /><img src=\"http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png\" />";

            return data;
        }
    }
}
