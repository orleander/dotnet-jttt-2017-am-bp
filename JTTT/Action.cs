﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{    
    public abstract class Action
    {
        public int Id { set; get; }
        public abstract void Execute(string data);
    }
}
