﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JTTT
{
    /// <summary>
    /// Logika interakcji dla klasy WeatherWindow.xaml
    /// </summary>
    public partial class WeatherWindow : Window
    {
        public WeatherWindow()
        {
            InitializeComponent();
        }

        private void check_Click(object sender, RoutedEventArgs e)
        {         
            WeatherModel weather = OpenWeather.GetWeatherForCity(city.Text);
            description.Text = "Teraz jest " + OpenWeather.ToCelsius(weather.main.temp) + " °C. Ciśnienie " + weather.main.pressure + " hPa. Pogoda: " + weather.weather[0].description + ".";

            image.Source = DownloadImage(weather.weather[0].icon);
        }

        private BitmapImage DownloadImage(string icon)
        {
            var image = new Image();
            var fullFilePath = "http://openweathermap.org/img/w/" + icon + ".png";

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(fullFilePath, UriKind.Absolute);
            bitmap.EndInit();

            return bitmap;
        }
    }
}
